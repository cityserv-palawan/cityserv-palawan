<?php 

namespace App\Laravel\Controllers\Api;

use App\Laravel\Models\UserLog;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\UserLogTransformer;


use Helper, Carbon, Session, Input, Str, Auth, URL, Cache;

class UserLogController extends Controller{
	protected $response;

	public function __construct(){
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		$this->user_id = Input::get('auth_id');
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function show($format = ""){
		try{
			$log_id = Input::get('log_id',0);
			$user_id = $this->user_id;
			$log = UserLog::where('id',$log_id)->where('user_id',$user_id)->first();

			$this->response_code = 200;
			$this->response['data'] = $this->transformer->transform($log,new UserLogTransformer,'item');
			$this->response['status'] = TRUE;
			$this->response['msg'] = "Log Details";
			$this->response['status_code'] = "LOG_DETAILS";

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function destroy($format = ""){
		try{

			$user_id = Input::get('user_id');
			$log_id = Input::get('log_id');

			$log = UserLog::where('id',$log_id)->where('user_id',$user_id)->first();

			if($log->delete()){
				$this->response_code = 200;
				$this->response['status'] = TRUE;
				$this->response['msg'] = "Log deleted succesfully.";
				$this->response['status_code'] = "LOGD_DELETED";
			}else{
				$this->response['status'] = FALSE;
				$this->response['msg'] = "Database server communication error.";
				$this->response['status_code']	= "DB_ERROR";
				$this->response_code = 504;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function index($format = ""){
		try{
			$per_page = Input::get('per_page',10);
			$user_id = $this->user_id;
			$category = Input::get('category');

			$logs = UserLog::where('user_id',$user_id)->where('sub_type',$category)->orderBy('created_at',"DESC")->paginate($per_page);
			
			$this->response['data'] = $this->transformer->transform($logs,new UserLogTransformer,'collection');
			$this->response['has_morepage'] = $logs->hasMorePages();
			$this->response['msg'] = "List of logs.";
			$this->response['status_code'] = "USER_LOGS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}
}