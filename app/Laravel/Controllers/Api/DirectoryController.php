<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\Directory;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\DirectoryTransformer;

use Input, Str;


class DirectoryController extends Controller{


	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function index($format = ""){
		try{
			$per_page = Input::get("per_page",10);
			$directories = Directory::where('status',"published")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($directories,new DirectoryTransformer,'collection');
			$this->response['has_morepage'] = $directories->hasMorePages();
			$this->response['msg'] = "List of directories.";
			$this->response['status_code'] = "DIRECTORY_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = ""){
		try{
			$directory_id = Input::get('directory_id');
			$directory = Directory::where('id',$directory_id)->where('status',"published")->first();

			$this->response['data'] = $this->transformer->transform($directory,new DirectoryTransformer,'item');
			$this->response['msg'] = "Directory Details.";
			$this->response['status_code'] = "DIRECTORY_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}