<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\Service;

use App\Laravel\Transformers\TransformerManager;

use Input, Str;
use Curl;
use Helper;

class WeatherController extends Controller{


	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function forecast($format = "json"){
		try{
			switch(Str::lower($format)){
				case 'json':
					$weather = Curl::to('http://api.openweathermap.org/data/2.5/forecast')
						        ->withData( ['appid' => "c73d63203e2d81f253abc78095a1467f",'lon' => "122.5621", 'lat' => "10.7202"])
						        ->asJson()
						        ->get();

					$this->response['msg'] = "Unable to fetch weather forecast";
					$this->response['status_code'] = "WEATHER_NOT_FOUND";
					$this->response['status'] = FALSE;

					if($weather){
						$lists = $weather->list;
						$this->response['weather_forecast'] = [];
						$this->response['msg'] = "Five day weather forecast";
						$this->response['status_code'] = "WEATHER_FORECAST";
						$this->response['status'] = TRUE;
						$this->response_code = 201;

						foreach ($lists as $key => $list) {
							$day = $list->dt_txt;
							if((strpos($day, "06:00:00") AND $key > 0) OR $key == 0){ //gets the weather forecast at 6:00 am for the succeeding days
								$daily_forecast = [	
													"day"		=> Helper::date_format($day,"D"), 
													"date" 		=> Helper::date_format($day,"Y-m-d"), 
													"time" 		=> Helper::date_format($day,"h:i a"),
													"kelvin" 	=> ceil($list->main->temp_min)." K",
													"fahrenheit"=> ceil($list->main->temp_min * 9 / 5 - 459.67)."°F",
													"celsius" 	=> ceil($list->main->temp_min - 273.15)."°C",
													"weather" 	=> [
														"icon"			=> $list->weather[0]->icon,
														"description"	=> $list->weather[0]->description
													]
												];
								array_push($this->response['weather_forecast'], $daily_forecast);
							}
						}
					}
					
					return response()->json($this->response,$this->response_code);

				break;

				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
			}
			return response()->json($this->response,$this->response_code);
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}
}