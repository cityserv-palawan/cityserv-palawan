<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Survey;

/**
*
* Requests used for validating inputs
*/

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class SurveyController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['surveys'] = Survey::select("*", DB::raw('sum(equivalent_value) AS evaluation'))->orderBy('updated_at',"DESC")->groupBy(DB::raw("CONCAT(user_id, '-', tracking_number)"))->get();
		return view('backoffice.surveys.index',$this->data);
	}

	public function show ($id = NULL) {
		$survey = Survey::find($id);

		if (!$survey) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.surveys.index');
		}

		$this->data['survey'] = $survey;
		$this->data['surveys'] = Survey::where('citizen_request_id', $survey->citizen_request_id)->where('user_id', $survey->user_id)->get();
		return view('backoffice.surveys.show',$this->data);
	}

	
}