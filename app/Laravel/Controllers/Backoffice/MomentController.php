<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Moment;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\MomentRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class MomentController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['pending' => "Pending", 'approved' => "Approved", "rejected" => "Rejected"];
	}

	public function index () {
		$this->data['moments'] = Moment::orderBy('updated_at',"DESC")->get();
		return view('backoffice.moments.index',$this->data);
	}

	public function edit ($id = NULL) {
		$moment = Moment::find($id);

		if (!$moment) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.moments.index');
		}

		$this->data['moment'] = $moment;
		return view('backoffice.moments.edit',$this->data);
	}

	public function update (MomentRequest $request, $id = NULL) {
		try {
			$moment = Moment::find($id);

			if (!$moment) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.moments.index');
			}

			$moment->fill($request->all());
			
			if($request->hasFile('file')) $moment->fill(ImageUploader::upload($request, 'uploads/reports',"file"));

			if($moment->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report has been updated.");
				return redirect()->route('backoffice.moments.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$moment = Moment::find($id);

			if (!$moment) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.moments.index');
			}

			if($moment->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report has been deleted.");
				return redirect()->route('backoffice.moments.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}