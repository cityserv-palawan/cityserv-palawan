<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Service;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ServiceRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class ServiceController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['services'] = Service::orderBy('updated_at',"DESC")->get();
		return view('backoffice.services.index',$this->data);
	}

	public function create () {
		return view('backoffice.services.create',$this->data);
	}

	public function store (ServiceRequest $request) {
		try {
			$new_service = new Service;
			$new_service->fill($request->all());
			$new_service->code = Helper::get_slug('service','title',$request->get('title'));

			if($new_service->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A service has been added.");
				return redirect()->route('backoffice.services.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$service = Service::find($id);

		if (!$service) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.services.index');
		}

		$this->data['service'] = $service;
		return view('backoffice.services.edit',$this->data);
	}

	public function update (ServiceRequest $request, $id = NULL) {
		try {
			$service = Service::find($id);

			if (!$service) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.services.index');
			}

			$service->fill($request->all());

			if($service->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A service has been updated.");
				return redirect()->route('backoffice.services.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$service = Service::find($id);

			if (!$service) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.services.index');
			}

			if($service->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A service has been deleted.");
				return redirect()->route('backoffice.services.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}