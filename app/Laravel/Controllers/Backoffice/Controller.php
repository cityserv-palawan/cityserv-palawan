<?php 

namespace App\Laravel\Controllers\Backoffice;

use Illuminate\Support\Collection;
use App\Laravel\Controllers\Controller as MainController;
use Auth, Session;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_backoffice_routes();
		self::set_user_info();
	}

	public function set_backoffice_routes(){
		$this->data['routes'] = array(
			"dashboard" => ['backoffice.dashboard'],
			"app_settings" => ['backoffice.app_settings.index','backoffice.app_settings.create','backoffice.app_settings.edit'],
			"articles" => ['backoffice.articles.index','backoffice.articles.create','backoffice.articles.edit'],
			"announcements" => ['backoffice.announcements.index','backoffice.announcements.create','backoffice.announcements.edit'],
			"polls" => ['backoffice.polls.index','backoffice.polls.create','backoffice.polls.edit'],
			"events" => ['backoffice.events.index','backoffice.events.create','backoffice.events.edit'],
			"users" => ['backoffice.users.index','backoffice.users.create','backoffice.users.edit'],
			"admins" => ['backoffice.admins.index','backoffice.admins.create','backoffice.admins.edit'],
			"citizen_reports" => ['backoffice.citizen_reports.index','backoffice.citizen_reports.create','backoffice.citizen_reports.edit'],
			"death_certificate_requests" => ['backoffice.death_certificate_requests.index','backoffice.death_certificate_requests.create','backoffice.death_certificate_requests.edit'],
			"marriage_certificate_requests" => ['backoffice.marriage_certificate_requests.index','backoffice.marriage_certificate_requests.create','backoffice.marriage_certificate_requests.edit'],
			"prior_adoption_requests" => ['backoffice.prior_adoption_requests.index','backoffice.prior_adoption_requests.create','backoffice.prior_adoption_requests.edit'],
			"after_adoption_requests" => ['backoffice.after_adoption_requests.index','backoffice.after_adoption_requests.create','backoffice.after_adoption_requests.edit'],
			"moments" => ['backoffice.moments.index','backoffice.moments.create','backoffice.moments.edit'],
			"directories" => ['backoffice.directories.index','backoffice.directories.create','backoffice.directories.edit'],
			"establishments" => ['backoffice.establishments.index','backoffice.establishments.create','backoffice.establishments.edit'],
			"emergencies" => ['backoffice.emergencies.index','backoffice.emergencies.create','backoffice.emergencies.edit'],
			"request_types" => ['backoffice.request_types.index','backoffice.request_types.create','backoffice.request_types.edit'],
			"citizen_requests" => ['backoffice.citizen_requests.index','backoffice.citizen_requests.create','backoffice.citizen_requests.edit'],
			"services" => ['backoffice.services.index','backoffice.services.create','backoffice.services.edit'],
			"subservices" => ['backoffice.subservices.index','backoffice.subservices.create','backoffice.subservices.edit'],
			"pages" => ['backoffice.pages.index','backoffice.pages.create','backoffice.pages.edit'],
			"widgets" => ['backoffice.widgets.index','backoffice.widgets.create','backoffice.widgets.edit'],
			"surveys" => ['backoffice.surveys.index','backoffice.surveys.show'],
			"reports" => ['backoffice.reports.index','backoffice.reports.generate'],
			"schools" => ['backoffice.schools.index','backoffice.schools.create','backoffice.schools.edit'],
			"report_footers" => ['backoffice.report_footers.index','backoffice.report_footers.create','backoffice.report_footers.edit'],
		);
	}

	public function set_user_info(){
		$this->data['auth'] = Auth::user();
	}
	
	public function get_data(){
		return $this->data;
	}
}