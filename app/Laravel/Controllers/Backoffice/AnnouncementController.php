<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Announcement;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AnnouncementRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class AnnouncementController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['announcements'] = Announcement::orderBy('updated_at',"DESC")->get();
		return view('backoffice.announcements.index',$this->data);
	}

	public function create () {
		return view('backoffice.announcements.create',$this->data);
	}

	public function store (AnnouncementRequest $request) {
		try {
			$new_announcement = new Announcement;
			$new_announcement->fill($request->all());
			$new_announcement->user_id = $this->data["auth"]->id;
			$new_announcement->posted_at = Helper::datetime_db($request->get('posted_at'));
			$new_announcement->slug = Helper::get_slug('article','title',$request->get('title'));
			$new_announcement->excerpt = Helper::get_excerpt($request->get('content'));
			$new_announcement->featured = ($request->get('featured',0) == 1) ? 1 : 0;
			if($request->hasFile('file')) $new_announcement->fill(ImageUploader::upload($request, 'uploads/announcement',"file"));

			if($new_announcement->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An announcement has been added.");
				return redirect()->route('backoffice.announcements.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$announcement = Announcement::find($id);

		if (!$announcement) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.announcements.index');
		}

		$this->data['announcement'] = $announcement;
		return view('backoffice.announcements.edit',$this->data);
	}

	public function update (AnnouncementRequest $request, $id = NULL) {
		try {
			$announcement = Announcement::find($id);

			if (!$announcement) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.announcements.index');
			}

			$old_title = $announcement->title;

			$announcement->fill($request->all());
			$announcement->posted_at = Helper::datetime_db($request->get('posted_at'));
			$announcement->excerpt = Helper::get_excerpt($request->get('content'));
			$announcement->featured = ($request->get('featured',0) == 1) ? 1 : 0;
			
			if($old_title != $request->get('title')){
				$announcement->slug = Helper::get_slug('article','title',$request->get('title'));
			}
			
			if($request->hasFile('file')) $announcement->fill(ImageUploader::upload($request, 'uploads/article',"file"));

			if($announcement->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An announcement has been updated.");
				return redirect()->route('backoffice.announcements.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$announcement = Announcement::find($id);

			if (!$announcement) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.announcements.index');
			}

			if($announcement->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An announcement has been deleted.");
				return redirect()->route('backoffice.announcements.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}