<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Establishment;
use App\Laravel\Models\Directory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\EstablishmentRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class EstablishmentController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['directories'] = ['' => "Choose directory"] + Directory::published()->orderBy('title',"ASC")->lists('title','id')->toArray();
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['establishments'] = Establishment::orderBy('updated_at',"DESC")->get();
		return view('backoffice.establishments.index',$this->data);
	}

	public function create () {
		$this->data['geo_lat'] = "14.5800";
		$this->data['geo_long'] = "121.0000";
		return view('backoffice.establishments.create',$this->data);
	}

	public function store (EstablishmentRequest $request) {
		try {
			$new_establishment = new Establishment;
			$new_establishment->fill($request->all());

			if($new_establishment->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An establishment has been added.");
				return redirect()->route('backoffice.establishments.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$establishment = Establishment::find($id);

		if (!$establishment) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.establishments.index');
		}

		$this->data['establishment'] = $establishment;
		$this->data['geo_lat'] = $establishment->geo_lat;
		$this->data['geo_long'] = $establishment->geo_long;
		return view('backoffice.establishments.edit',$this->data);
	}

	public function update (EstablishmentRequest $request, $id = NULL) {
		try {
			$establishment = Establishment::find($id);

			if (!$establishment) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.establishments.index');
			}

			$establishment->fill($request->all());

			if($establishment->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An establishment has been updated.");
				return redirect()->route('backoffice.establishments.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$establishment = Establishment::find($id);

			if (!$establishment) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.establishments.index');
			}

			if($establishment->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An establishment has been deleted.");
				return redirect()->route('backoffice.establishments.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}