<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Page;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PageRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class PageController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['pages'] = Page::orderBy('updated_at',"DESC")->get();
		return view('backoffice.pages.index',$this->data);
	}

	public function create () {
		return view('backoffice.pages.create',$this->data);
	}

	public function store (PageRequest $request) {
		try {
			$new_page = new Page;
			$new_page->fill($request->all());
			$new_page->slug = Helper::get_slug('page','title',$request->get('title'));
			$new_page->code = $new_page->slug;

			$new_page->excerpt = Helper::get_excerpt($request->get('content'));

			if($new_page->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A page has been added.");
				return redirect()->route('backoffice.pages.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$page = Page::find($id);

		if (!$page) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.pages.index');
		}

		$this->data['page'] = $page;
		return view('backoffice.pages.edit',$this->data);
	}

	public function update (PageRequest $request, $id = NULL) {
		try {
			$page = Page::find($id);

			if (!$page) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.pages.index');
			}

			$old_title = $page->title;

			$page->fill($request->all());
			$page->excerpt = Helper::get_excerpt($request->get('content'));

			if($old_title != $request->get('title')){
				$page->slug = Helper::get_slug('page','title',$request->get('title'));
				$page->code = $pag->slug;
			}
			
			if($page->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A page has been updated.");
				return redirect()->route('backoffice.pages.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$page = Page::find($id);

			if (!$page) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.pages.index');
			}

			if($page->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A page has been deleted.");
				return redirect()->route('backoffice.pages.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}