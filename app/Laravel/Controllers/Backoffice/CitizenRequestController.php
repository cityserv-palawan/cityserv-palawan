<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\MacRequest;
use App\Laravel\Models\User;
use App\Laravel\Models\UserLog;
use App\Laravel\Models\CRModule;
use App\Laravel\Models\CRCategory;
use App\Laravel\Models\CRSubCategory;
use App\Laravel\Models\CRProcess;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\Queue;
use App\Laravel\Models\UserTracker;
use App\Laravel\Models\School;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CRRequest;
use App\Laravel\Requests\Backoffice\TrackerRequest;
use App\Laravel\Requests\Backoffice\TrackerQuickUpdateRequest;

/**
*
* Additional classes needed by this controller
*/
use Event;
use App\Laravel\Events\LogCitizenRequest;
use App\Laravel\Events\FCMNotification;

use Helper, ImageUploader, Carbon, Session, Str, DB, Input;

class CitizenRequestController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "Choose status", 'done' => "Done", 'rejected' => "Rejected"];
		
		if($this->data['auth']->zone AND $this->data['auth']->zone != "0") {
			$this->data['zones'] = ['' => "Choose zone", "{$this->data['auth']->zone}" => "Zone {$this->data['auth']->zone}"];
		} else {
			$this->data['zones'] = ['' => "Choose zone", 'I' => "Zone I", 'II' => "Zone II", 'III' => "Zone III"];
		}


		$this->data['users'] = ['' => "Choose citizen"] + User::select(DB::raw("CONCAT(COALESCE(`fname`,''),' ',COALESCE(`lname`,''),' ( ',COALESCE(`email`,''),' ) - Brgy. ',COALESCE(`barangay`,'---')) AS display_name"),'id')->where('type',"user")->orderBy('updated_at',"DESC")->get()->lists('display_name','id')->toArray();

		$this->data['types'] = [
									'' => "Choose type", 
									'MAYORS ACTION CENTER'	=> [
											'SIMPLE TRANSFER (TR)|assessor_simple' => "SIMPLE TRANSFER (TR)",
											'Public and Private Hospitals|hospitals' => "Public and Private Hospitals",
											'Regional Office DSWD|regional_dswd' => "Regional Office DSWD",
											'Local Civil Registry|local_registry' => "Local Civil Registry",
											'Public and Private Schools and Universities|schools' => "Public and Private Schools and Universities",
											'TESDA|tesda' => "TESDA",
									],
									// 'SIMPLE TRANSACTION w/o TAXMAPPING' => [
									// 	'SIMPLE TRANSFER (TR)|assessor_simple' => "SIMPLE TRANSFER (TR)",
									// 	'REASSESSMENT DUE TO DISPUTE (DP)|assessor_simple' => "REASSESSMENT DUE TO DISPUTE (DP)",
									// 	'ENTER TO IDLE (EI)|assessor_simple' => "ENTER TO IDLE (EI)",
									// 	'DROP TO IDLE (DI)|assessor_simple' => "DROP TO IDLE (DI)",
									// 	'TAXABLE TO EXEMPT (TE)|assessor_simple' => "TAXABLE TO EXEMPT (TE)",
									// 	'EXEMPT TO TAXABLE (ET)|assessor_simple' => "EXEMPT TO TAXABLE (ET)",
									// 	'PHYSICAL CHANGE (PC)|assessor_simple' => "PHYSICAL CHANGE (PC)",
									// ],

									// 'TRANSACTION with TAXMAPPING' => [
									// 	'DISCOVERY/NEW DECLARATION (DC)|assessor_with_taxmapping' => "DISCOVERY/NEW DECLARATION (DC)",
									// 	'SUBDIVISION (SD)|assessor_with_taxmapping' => "SUBDIVISION (SD)",
									// 	'RECLASSIFICATION (RC)|assessor_with_taxmapping' => "RECLASSIFICATION (RC)",
									// 	'CONSOLIDATION (CS)|assessor_with_taxmapping' => "CONSOLIDATION (CS)",
									// 	'REASSESSMENT DUE TO DISPUTE (DP) (RECTIFICATION OF PIN)|assessor_with_taxmapping' => "REASSESSMENT DUE TO DISPUTE (DP) (RECTIFICATION OF PIN)",
									// 	'CANCELLATION OF REAL PROPERTY (CA)|assessor_with_taxmapping' => "CANCELLATION OF REAL PROPERTY (CA)",
									// ],
									
								];

		$categories = CRCategory::orderBy('title',"ASC")->get();
		$subcategories = array();
		foreach ($categories as $index => $category) {
			$subcategories[$category->title] = CRSubcategory::where('cr_category_id', $category->id)->lists('title','code')->toArray();
		}
		$this->data['modules'] = ['' => "Choose module"] + CRModule::orderBy('title',"ASC")->lists('title','code')->toArray();
		$this->data['categories'] = ['' => "Choose category"] + $categories->lists('title', 'code')->toArray();
		$this->data['subcategories'] = ['' => "Choose MAC Category"] + $subcategories;
		$this->data['genders'] = ['' => "Choose Gender", 'male' => "Male", 'female' => "Female"];
		$this->data['civil_statuses'] = ['' => "Choose Civil Status", 'single' => "Single", 'married' => "Married", 'divorced' => "Divorced", 'separated' => "Separated", 'widowed' => "Widowed"];
		$this->data['schools'] = ['' => "Choose school/university"] + School::orderBy('title',"ASC")->lists('title','id')->toArray();
	}

	public function index () {

		$now = Carbon::now();
		$date_range = Input::get('date_range', Helper::date_db($now) . " - " . Helper::date_db($now) );
		$date_range_array = explode(" - ", $date_range);

		$this->data['requests'] = CitizenRequest::whereHas('tracker', function($query) use ($date_range_array) {
									$query->whereBetween(DB::raw("DATE (appointment_schedule)"), $date_range_array);
								})->orderBy('created_at', "DESC")->get();

		$this->data['date_range'] = $date_range;
		return view('backoffice.citizen-requests.index',$this->data);
	}

	public function create () {
		return view('backoffice.citizen-requests.create',$this->data);
	}

	public function store (CRRequest $request) {
		try {
			

			DB::beginTransaction();
			$success = true;
			// $user = User::select(DB::raw("CONCAT_WS(' ', fname, lname) AS name"), "contact_number", "age", "gender")->find($request->get('user_id'));

			try {

				// Fetch request info
				// $cr_module = CRModule::where('code', $request->get('module', "mac"))->first();
				// $cr_category = CRCategory::where('code', $request->get('category'))->first();
				$cr_subcategory = CRSubCategory::where('code', $request->get('subcategory'))->first();
				$cr_processes = CRProcess::where('cr_subcategory_id', $cr_subcategory->id)->orderBy('sequence',"ASC")->get();
				$cr_category = CRCategory::where('id', $cr_subcategory->cr_category_id)->first();
				$cr_module = CRModule::where('id', $cr_category->cr_module_id)->first();

				// Create new citizen request
				$new_request = new CitizenRequest;
				$new_request->fill($request->all());
				$new_request->module = $cr_module->code;
				$new_request->category = $cr_category->code;
				$new_request->subcategory = $cr_subcategory->code;
				// $new_request->user_id = 0;
				$new_request->user_id = $request->get('user_id');
				$new_request->status = "pending";
				$new_request->remarks = "Request created.";
				$new_request->title = $cr_subcategory->title;
				$new_request->code = Helper::create_request_code($cr_category->id, $request->get('appointment_schedule', Carbon::now()));

				$suffix = $cr_subcategory->id; //For now, use the primary key of the subcategory to determine the suffix
				$new_request->year = Carbon::now()->format('y');
				$new_request->sequence_no = Helper::next_tracking_sequence($new_request->subcategory, $new_request->year);
				$new_request->tracking_number = Helper::create_tracking_number(['year' => $new_request->year, 'sequence_no' => $new_request->sequence_no, 'suffix' => $suffix]);
				$new_request->save();

				// Create tracker header
				$header = new CRTrackerHeader;
				$header->citizen_request_id = $new_request->id;
				$header->cr_category_id = $cr_category->id;
				$header->cr_subcategory_id = $cr_subcategory->id;
				$header->category_title = $cr_category->title;
				$header->category_code = $cr_category->code;
				$header->subcategory_title = $cr_subcategory->title;
				$header->subcategory_code = $cr_subcategory->code;
				$header->appointment_schedule = $request->get('appointment_schedule', Carbon::now()); 
				$header->save();

				// Create copy of processes to tracker table
				foreach ($cr_processes as $index => $process) {
					$tracker = new CRTracker;
					$tracker->cr_tracker_header_id = $header->id;
					$tracker->cr_process_id = $process->id;
					$tracker->process_title = $process->title;
					$tracker->process_code = $process->code;
					if($index == 0) $tracker->from = Carbon::now();
					$tracker->save();
				}

				// Create queue
				$new_queue = new Queue;
				$new_queue->service_type = $cr_category->code;
				$new_queue->sequence_no = Helper::next_sequence($cr_category->code);
				$new_queue->queue_no = Helper::create_queue_no($cr_category->code, $new_queue->sequence_no, "mobile");
				$new_queue->status = "pending";
				$new_queue->source = "mobile";
				$new_queue->save();

				DB::commit();

			} catch (Exception $e) {
				DB::rollBack();
				$success = false;
			}

			if($success) {

				$data = new LogCitizenRequest(['citizen_request_id' => $new_request->id, 'data' => ['user_id' => $this->data['auth']->id, 'remarks' => $new_request->remarks]]);
				Event::fire('log-citizen-request', $data);

				// $new_user_log = new UserLog;
				// $new_user_log->fill(['user_id' => $this->data['auth']->user_id, 'msg' => "A request has been submitted.", 'type' => "MAC", 'sub_type' => $cr_category->code, 'reference_id' => $new_request->id, 'title' => "Request submitted."]);
				// $new_user_log->save();

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request has been created.");
				return redirect()->route('backoffice.citizen_requests.index');

			} else {
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Something went wrong.');
			}

			// $new_request = new CitizenRequest;
			// $new_request->fill($request->all());

			// $new_request->user_id = $request->get('user_id');
			// $new_request->status = "pending";
			// $new_request->remarks = "Request submitted.";

			// if($new_request->save()) {

			// 	$new_request->code = "REQ" . str_pad($new_request->id, 8, 0, STR_PAD_LEFT);

			// 	$type_data = explode("|", $request->get('request_type'));

			// 	if(count($type_data) >= 2){

			// 		if(in_array($type_data[1], ["assessor_simple","assessor_with_taxmapping"])) {
						
			// 			$new_request->type = "assessor";
			// 			$new_request->sub_type = $type_data[1];
			// 			$new_request->title = $type_data[0];

			// 			$assessor_request = NULL;

			// 			switch ($type_data[1]) {
			// 				case 'assessor_simple':
			// 					$new_request->target_table = "assessor_request_simple";
			// 					$assessor_request = new AssessorRequestSimple;
			// 				break;

			// 				case 'assessor_with_taxmapping':
			// 					$new_request->target_table = "assessor_request_with_taxmapping";
			// 					$assessor_request = new AssessorRequestWithTaxmapping;
			// 				break;
			// 			}

			// 			if($assessor_request != NULL) {
			// 				$assessor_request->citizen_request_id = $new_request->id;
			// 				$assessor_request->fill($request->all());


			// 				if($new_request->sub_type == "assessor_simple"){
			// 					$valid_tasks = ['doc_inspection','doc_evaluation','recommendation','final_approval',"logofind","assessment_record"];
			// 				}else{
			// 					$valid_tasks = ['office_order_preparation','office_order_approval','doc_inspection','taxmapping','doc_evaluation','recommendation','final_approval',"logofind","assessment_record"];
			// 				}


			// 				$assessor_request->{$valid_tasks[0] . "_from"} =  Carbon::now();

			// 				$assessor_request->save();
			// 			}
			// 		}

			// 	}

			// 	$new_request->save();

			// 	$data = new LogCitizenRequest(['citizen_request_id' => $new_request->id, 'data' => ['user_id' => $this->data['auth']->id, 'remarks'=> $new_request->remarks]]);
			// 	Event::fire('log-citizen-request', $data);

			// 	Session::flash('notification-status','success');
			// 	Session::flash('notification-msg',"A request has been created.");
			// 	return redirect()->route('backoffice.citizen_requests.index');
			// }

			// Session::flash('notification-status','failed');
			// Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$citizen_request = CitizenRequest::find($id);

		if (!$citizen_request) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.citizen_requests.index');
		}

		$this->data['request'] = $citizen_request;
		$this->data['subcategory'] = CRSubcategory::where('code', $citizen_request->subcategory)->first();
		return view('backoffice.citizen-requests.edit',$this->data);
	}

	public function transaction ($id = NULL) {
		$citizen_request = CitizenRequest::find($id);

		if (!$citizen_request) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.citizen_requests.index');
		}

		$this->data['request'] = $citizen_request;
		$this->data['processes'] = $citizen_request->tracker->info();
		return view('backoffice.citizen-requests.transaction',$this->data);
	}

	public function update (CRRequest $request, $id = NULL) {
		try {
			$citizen_request = CitizenRequest::find($id);

			if (!$citizen_request) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.citizen_requests.index');
			}

			$citizen_request->fill($request->all());
			
			if($citizen_request->save()) {

				$data = new LogCitizenRequest(['citizen_request_id' => $citizen_request->id, 'data' => ['user_id' => $this->data['auth']->id, 'remarks' => "Details of request has been updated."]]);
				Event::fire('log-citizen-request', $data);


				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request has been updated.");
				return redirect()->route('backoffice.citizen_requests.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$citizen_request = CitizenRequest::find($id);

			if (!$citizen_request) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.citizen_requests.index');
			}

			if($citizen_request->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request has been deleted.");
				return redirect()->route('backoffice.citizen_requests.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function tracker_edit($id = NULL){

		$tracker = CRTracker::with('header.citizen_request')->find($id);

		if( !$tracker OR !($tracker->header AND $tracker->header->citizen_request) ) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.citizen_request.index');
		}

		$processes = $tracker->header ? $tracker->header->info()->get() : array();
		$next_process = $processes->where('id', $tracker->id + 1)->first();
		$previous_process = $processes->where('id', $tracker->id - 1)->first();

		if($previous_process AND $previous_process->status == "pending") {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"There are pending tasks before you can update the chosen task.");
			return redirect()->back();
		}

		if(!in_array($tracker->status, ['pending','for_revalidation'] )) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"The chosen task is already processed.");
			return redirect()->back();
		}

		$this->data['tracker'] = $tracker;
		$this->data['request'] = $tracker->header->citizen_request;
		$this->data['is_last_process'] = $next_process ? FALSE : TRUE;

		return view('backoffice.citizen-requests.tracker',$this->data);


		// $citizen_request = CitizenRequest::find($id);

		// // Section subject for code optimization
		// if (!$citizen_request) {
		// 	Session::flash('notification-status',"failed");
		// 	Session::flash('notification-msg',"Record not found.");
		// 	return redirect()->route('backoffice.citizen_requests.index');
		// }

		// $mac_request = $citizen_request->mac_info;

		// if (!$mac_request) {
		// 	Session::flash('notification-status',"failed");
		// 	Session::flash('notification-msg',"Record not found.");
		// 	return redirect()->route('backoffice.citizen_requests.transact',[$citizen_request->id]);
		// }

		// $task = Str::lower(Input::get('task'));

		// switch($citizen_request->type){
		// 	case 'mac':
		// 		$valid_tasks = ['info_validation','assessment_evaluation','final_interview','requirement_submission'];
		// 	break;
		// }
		// // if($citizen_request->sub_type == "assessor_simple"){
		// // 	$valid_tasks = ['doc_inspection','doc_evaluation','recommendation','final_approval',"logofind","assessment_record"];
		// // }else{
		// // 	$valid_tasks = ['office_order_preparation','office_order_approval','doc_inspection','taxmapping','doc_evaluation','recommendation','final_approval',"logofind","assessment_record"];
		// // }

		// if(!in_array($task, $valid_tasks)) {
		// 	Session::flash('notification-status',"failed");
		// 	Session::flash('notification-msg',"Invalid task.");
		// 	return redirect()->back();
		// }

		// $index = array_search($task, $valid_tasks);
		// if($index > 0) {
		// 	if($mac_request->{$valid_tasks[$index-1] . "_status"} != "done"){
		// 		Session::flash('notification-status',"failed");
		// 		Session::flash('notification-msg',"There are pending tasks before you can update the chosen task.");
		// 		return redirect()->back();
		// 	}
		// }

		// if(!in_array($mac_request->{$task . "_status"}, ['pending','for_revalidation'] )) {
		// 	Session::flash('notification-status',"failed");
		// 	Session::flash('notification-msg',"The chosen task is already processed.");
		// 	return redirect()->back();
		// }
		// // End of section

		// $this->data['request'] = $citizen_request;
		// $this->data['task'] = $task;
		// return view('backoffice.citizen-requests.tracker',$this->data);
	}

	public function tracker_update(TrackerRequest $request, $id = NULL){
		try {

			$tracker = CRTracker::with('header.citizen_request')->find($id);

			if( !$tracker OR !($tracker->header AND $tracker->header->citizen_request) ) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.citizen_request.index');
			}

			$header = $tracker->header;
			$citizen_request = $tracker->header->citizen_request;
			$processes = $tracker->header ? $tracker->header->info()->get() : array();
			$next_process = $processes->where('id', $tracker->id + 1)->first();
			$previous_process = $processes->where('id', $tracker->id - 1)->first();

			if($previous_process AND $previous_process->status == "pending") {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"There are pending tasks before you can update the chosen task.");
				return redirect()->back();
			}

			if(!in_array($tracker->status, ['pending','for_revalidation'] )) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"The chosen task is already processed.");
				return redirect()->back();
			}

			try {

				DB::beginTransaction();

				$super_user = $this->data['auth']->type == "super_user" ? true : false;

				$tracker->user_id = $this->data['auth']->id;
				$tracker->status = ($request->get('status') == "done") ? "done" : "rejected";
				$tracker->remarks = $request->get('remarks');
				$tracker->from =  $super_user ? $request->get('from') : $tracker->from;
				$tracker->to = $super_user ? $request->get('to') : Carbon::now();

				$from = Carbon::parse($tracker->from);
				$to = Carbon::parse($tracker->to);
				$mins_passed = ceil((strtotime($to) - strtotime($from)) / 60);
				$tracker->duration += $mins_passed;

				$tracker->save();

				$citizen_request->remarks = $request->get('remarks');
				$citizen_request->save();

				$header->release_amount = $request->get('release_amount', 0.00);
				$header->save();

				if($request->get('status') == "done"){
					if($next_process) {
						// Reset next process if available ...
						$next_process->update(['user_id' => 0, 'status' => "pending", 'from' => $tracker->to, 'to' => NULL]);
					} else {
						// otherwise, mark citizen request as completed.
						$citizen_request->update(['status' => "done"]);
					}
				} else {
					if($previous_process)  {
						// Mark previous process as 'for_revalidation' if available
						$previous_process->update(['user_id' => 0, 'status' => "for_revalidation", 'from' => $tracker->to, 'to' => NULL]);
					} else {
						// otherwise, mark citizen request as rejected.
						$citizen_request->update(['status' => "rejected"]);
					}
				}

				$data = new LogCitizenRequest(['citizen_request_id' => $citizen_request->id, 'data' => ['user_id' => $this->data['auth']->id,'remarks' => $request->get('remarks')]]);
				Event::fire('log-citizen-request', $data);

				// $user_ids = UserTracker::select('user_id')->where('citizen_request_id',$citizen_request->id)->groupBy('user_id')->lists('user_id');
				// foreach ($user_ids as $user_id) {
				// 	$title = $tracker->process_title . " has been " .( $tracker->status == "done" ? "approved." : "declined.");
				// 	$data = new FCMNotification(['auth_id' => $user_id, 'reference_id' => $citizen_request->id, 'type' => "MAC", 'title' => $title, 'msg' => $request->get('remarks'), 'sub_type' => $citizen_request->subcategory, 'thumbnail' => ""]);
				// 	Event::fire('fcm-notification', $data);
				// }

				$title = $tracker->process_title . " has been " .( $tracker->status == "done" ? "approved." : "declined.");
				$data = new FCMNotification(['auth_id' => $citizen_request->user_id, 'reference_id' => $citizen_request->id, 'type' => "MAC", 'title' => $title, 'msg' => $request->get('remarks'), 'sub_type' => $citizen_request->category, 'thumbnail' => ""]);
				Event::fire('fcm-notification', $data);

				DB::commit();

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A task has been updated.");
				return redirect()->route('backoffice.citizen_requests.transaction',[$citizen_request->id]);

			} catch (Exception $e) {
				DB::rollback();
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Something went wrong.');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function tracker_quick_update(TrackerQuickUpdateRequest $request, $id = NULL){
		try {

			$tracker = CRTracker::with('header.citizen_request')->find($id);

			if( !$tracker OR !($tracker->header AND $tracker->header->citizen_request) ) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.citizen_request.index');
			}

			$header = $tracker->header;
			$citizen_request = $tracker->header->citizen_request;
			$processes = $tracker->header ? $tracker->header->info()->get() : array();
			$next_process = $processes->where('id', $tracker->id + 1)->first();
			$previous_process = $processes->where('id', $tracker->id - 1)->first();

			if($previous_process AND $previous_process->status == "pending") {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"There are pending tasks before you can update the chosen task.");
				return redirect()->back();
			}

			if(!in_array($tracker->status, ['pending','for_revalidation'] )) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"The chosen task is already processed.");
				return redirect()->back();
			}

			try {

				DB::beginTransaction();

				$super_user = $this->data['auth']->type == "super_user" ? true : false;

				if($request->get('status') == "done") {
					$remarks = $tracker->process_title . " done.";
				} else {
					$remarks = $request->get('remarks');
				}

				$tracker->user_id = $this->data['auth']->id;
				$tracker->status = ($request->get('status') == "done") ? "done" : "rejected";
				$tracker->remarks = $remarks;
				$tracker->from =  $tracker->from;
				$tracker->to = Carbon::now();

				$from = Carbon::parse($tracker->from);
				$to = Carbon::parse($tracker->to);
				$mins_passed = ceil((strtotime($to) - strtotime($from)) / 60);
				$tracker->duration += $mins_passed;

				$tracker->save();

				$citizen_request->remarks = $remarks;
				$citizen_request->save();

				$header->release_amount = $request->get('release_amount', 0.00);
				$header->save();

				if($request->get('status') == "done"){
					if($next_process) {
						// Reset next process if available ...
						$next_process->update(['user_id' => 0, 'status' => "pending", 'from' => $tracker->to, 'to' => NULL]);
					} else {
						// otherwise, mark citizen request as completed.
						$citizen_request->update(['status' => "done"]);
					}
				} else {
					if($previous_process)  {
						// Mark previous process as 'for_revalidation' if available
						$previous_process->update(['user_id' => 0, 'status' => "for_revalidation", 'from' => $tracker->to, 'to' => NULL]);
					} else {
						// otherwise, mark citizen request as rejected.
						$citizen_request->update(['status' => "rejected"]);
					}
				}

				$data = new LogCitizenRequest(['citizen_request_id' => $citizen_request->id, 'data' => ['user_id' => $this->data['auth']->id,'remarks' => $remarks]]);
				Event::fire('log-citizen-request', $data);

				// $user_ids = UserTracker::select('user_id')->where('citizen_request_id',$citizen_request->id)->groupBy('user_id')->lists('user_id');
				// foreach ($user_ids as $user_id) {
				// 	$title = $tracker->process_title . " has been " .( $tracker->status == "done" ? "approved." : "declined.");
				// 	$data = new FCMNotification(['auth_id' => $user_id, 'reference_id' => $citizen_request->id, 'type' => "MAC", 'title' => $title, 'msg' => $remarks, 'sub_type' => $citizen_request->subcategory, 'thumbnail' => ""]);
				// 	Event::fire('fcm-notification', $data);
				// }

				$title = $tracker->process_title . " has been " .( $tracker->status == "done" ? "approved." : "declined.");
				$data = new FCMNotification(['auth_id' => $citizen_request->user_id, 'reference_id' => $citizen_request->id, 'type' => "MAC", 'title' => $title, 'msg' => $remarks, 'sub_type' => $citizen_request->category, 'thumbnail' => ""]);
				Event::fire('fcm-notification', $data);

				DB::commit();

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A task has been updated.");
				return redirect()->route('backoffice.citizen_requests.transaction',[$citizen_request->id]);

			} catch (Exception $e) {
				DB::rollback();
				Session::flash('notification-status','failed');
				Session::flash('notification-msg','Something went wrong.');
			}

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}