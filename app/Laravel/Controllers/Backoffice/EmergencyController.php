<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Emergency;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\EmergencyRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class EmergencyController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['emergencies'] = Emergency::orderBy('updated_at',"DESC")->get();
		return view('backoffice.emergencies.index',$this->data);
	}

	public function create () {
		return view('backoffice.emergencies.create',$this->data);
	}

	public function store (EmergencyRequest $request) {
		try {
			$new_emergency = new Emergency;
			$new_emergency->fill($request->all());

			if($new_emergency->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An emergency has been added.");
				return redirect()->route('backoffice.emergencies.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$emergency = Emergency::find($id);

		if (!$emergency) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.emergencies.index');
		}

		$this->data['emergency'] = $emergency;
		return view('backoffice.emergencies.edit',$this->data);
	}

	public function update (EmergencyRequest $request, $id = NULL) {
		try {
			$emergency = Emergency::find($id);

			if (!$emergency) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.emergencies.index');
			}

			$emergency->fill($request->all());

			if($emergency->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An emergency has been updated.");
				return redirect()->route('backoffice.emergencies.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$emergency = Emergency::find($id);

			if (!$emergency) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.emergencies.index');
			}

			if($emergency->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An emergency has been deleted.");
				return redirect()->route('backoffice.emergencies.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}