<?php

namespace App\Laravel\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use Session;

class ApiRequestManager extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	public function response(array $errors){

		$segments = $this->segments();
		$last_segment = explode(".", array_pop($segments));

		if(count($last_segment) == 2){
			switch($last_segment[1]){
				case 'json' :
					$response['msg'] = "Incomplete or invalid input data";
					$response['status_code'] = "INCOMPLETE_DATA";
					$response['status'] = FALSE;
					$response['errors'] = $errors;
					$response_code = 422;

					return response()->json($response,$response_code);
				break;

				default :
					$response['msg'] = "Invalid return data format.";
					$response['status_code'] = "INVALID_FORMAT";
					$response['status'] = FALSE;
					$response_code = 406;
			}
			
			return response()->json($response,$response_code);

		}
	}
}