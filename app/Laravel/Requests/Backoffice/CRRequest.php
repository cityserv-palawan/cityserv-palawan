<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class CRRequest extends RequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		$id = $this->id ? : 0;

		$rules = [
			'user_id' => "required|exists:user,id,type,user",
			// 'category' => "required|exists:cr_category,code",
			'subcategory' => "required|exists:cr_subcategory,code",
			// 'module' => "required|exists:cr_module,code",
			'name' => "required",
			'beneficiary_age' => "numeric",
			'beneficiary_income' => "numeric",
			// 'appointment_schedule' => "required",
		];

		if($this->get('subcategory') == "scholarship") {
			$rules['school_id'] = "required|exists:school,id";
		}

		if($id) {
			// unset($rules['subcategory']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "Field is required.",
			'school_id.required' => "Field is required when MAC Category is Scholarship.",
			'school_id.exists' => "Invalid school.",
			'user_id.exists' => "Invalid user.",
			// 'category.exists' => "Invalid category.",
			'subcategory.exists' => "Invalid MAC Category.",
			// 'module.exists' => "Invalid module.",
		];
	}
}