<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ReportRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'subcategory' => "required|exists:cr_subcategory,code",
			'status' => "required|in:completed,pending",
			'date_range' => "required",
		];

		if($this->get('subcategory') == "scholarship") {
			$rules['school_id'] = "required|exists:school,id";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'subcategory.exists' => "Type does not exist",
			'school_id.required' => "Field is required when MAC Category is Scholarship.",
			'school_id.exists' => "Type does not exist",
			'subcategory.in' => "Type does not exist",
		];
	}
}