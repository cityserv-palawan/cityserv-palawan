<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class SubServiceRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'service_id' => "required|exists:service,id",
			'title' => "required",
			'status' => "required|in:draft,published",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'status.in' => "Invalid status.",
			'service_id.exists' => "Invalid service",
		];
	}
}