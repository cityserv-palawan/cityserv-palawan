<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
use Input;

class FbInfoRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

			$rules = [
				'fname' 		=> "required",
				'lname' 		=> "required",
				'fb_id'			=> "required",
				'contact_number'	=> "required",
			];
		
		return $rules;
		
	}

	public function messages(){
		return [
			'fb_id.unique_fb'			=> "Facebook account is already connected to someone's acount.",
			'fb_id.required'			=> "Facebook account is required.",
			'required'					=> "Field is required.",

		];
	}

}