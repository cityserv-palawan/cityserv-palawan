<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class PasswordRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'current_password' 		=> "required|old_password:".$this->get('auth_id'),
			'password'				=> 	"required|password_format|between:6,25|confirmed",
		];
	}

	public function messages(){
		return [
			'old_password'				=> "Current password invalid.",
			'required'						=> "Field is required.",
			'password.between'			=> "Password must be min. of 8 and max. of 25 characters.",
			'password.confirmed'		=> "Password does not match.",
			'password.password_format'	=> "Must contain at least one letter and one number and a special character.",
		];
	}

}