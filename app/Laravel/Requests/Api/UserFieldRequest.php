<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
use Input;

class UserFieldRequest extends ApiRequestManager {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		switch($this->get('field')){
			case 'username':
				return ['value' => 'required|unique:user,username,'.$this->get('user_id')];
			break;
			case 'email':
				return ['value' => 'required|email|unique:user,email,'.$this->get('user_id')];
			break;
			case 'fname':
			case 'lname':
			case 'gender':
				return ['value' => "required"];
			break;
			default:
			return [];
		}

	}

	

	public function response(array $errors){
		$response['msg'] = "Incomplete or invalid input data";
		$response['status_code'] = "INCOMPLETE_DATA";
		$response['status'] = FALSE;
		$response['errors'] = $errors;
		$response_code = 422;

		return response()->json($response,$response_code);
	}

}
