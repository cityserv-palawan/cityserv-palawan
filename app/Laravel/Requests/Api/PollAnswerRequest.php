<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class PollAnswerRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'choice' => 'required',
			'poll_id' => 'required'
		];
	}

	public function messages(){
		return [
			'required' => "Field is required.",
		];
	}
}