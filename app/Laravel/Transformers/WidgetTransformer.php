<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Widget;

use App\Laravel\Transformers\CompactWidgetTransformer;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;
use Curl;

class WidgetTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','children'
    ];

	public function transform(Widget $widget){

	     return [
	     	'id' => $widget->id,
	     	'title' => $widget->title,
			'code' => $widget->code,
			'status' => $widget->status,
			
			'parent_id' => $widget->parent_id,
			'is_parent' => $widget->is_parent ? TRUE : FALSE,
			'type' => $widget->type,
			'display_screen' => $widget->display_screen,
			'short_description' => $widget->short_description,
			'content' => $widget->content,
			'list_type' => $widget->list_type,
			'display_type' => $widget->display_type,

			'image' => [
				'path' => $widget->directory,
	 			'filename' => $widget->filename,
	 			'path' => $widget->path,
	 			'directory' => $widget->directory,
	 			'full_path' => $widget->path ? "{$widget->directory}/resized/{$widget->filename}" : asset("{$widget->directory}/resized/{$widget->filename}"),
	 			'thumb_path' => $widget->path ? "{$widget->directory}/thumbnails/{$widget->filename}" : asset("{$widget->directory}/thumbnails/{$widget->filename}"),
 			]
	     ];
	}

	public function includeDate(Widget $widget){
        $collection = Collection::make([
			'date_db' => $widget->date_db($widget->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $widget->month_year($widget->created_at),
			'time_passed' => $widget->time_passed($widget->created_at),
			'timestamp' => $widget->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeChildren(Widget $widget){
		$children = $widget->children()->get();
		return $this->collection($children, new CompactWidgetTransformer);
	}

}