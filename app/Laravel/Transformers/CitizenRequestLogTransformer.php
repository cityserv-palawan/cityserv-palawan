<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\CitizenRequestLog;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class CitizenRequestLogTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date'
    ];

	public function transform(CitizenRequestLog $request){
	     return [
	     	'id' => $request->id,
	     	'user_id' => $request->user_id,
			'citizen_request_id' => $request->citizen_request_id,
			'remarks' => $request->remarks,
	     ];
	}

	public function includeDate(CitizenRequest $request){
        $collection = Collection::make([
			'date_db' => $request->date_db($request->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $request->month_year($request->created_at),
			'time_passed' => $request->time_passed($request->created_at),
			'timestamp' => $request->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}
}