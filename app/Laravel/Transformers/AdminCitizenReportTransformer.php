<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\CitizenReport;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class AdminCitizenReportTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date','user'
    ];

	public function transform(CitizenReport $report){
	     return [
	     	'id' => $report->id,
	     	'type'	=> $report->type,
	     	'excerpt' => Helper::get_excerpt($report->content),
	     ];
	}

	public function includeDate(CitizenReport $report){
        $collection = Collection::make([
			'date_db' => $report->date_db($report->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $report->month_year($report->created_at),
			'time_passed' => $report->time_passed($report->created_at),
			'timestamp' => $report->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(CitizenReport $report){
		$collection = Collection::make([
			'sender'	=> $report->author ? "{$report->author->fname} {$report->author->lname}" : "Anonymous",
			'status'	=> $report->status,
			'code'	=> $report->code,
			'content' => $report->content,
			'geolocation'	=> [
    			"lat"			=> $report->geo_long,
    			"long"			=> $report->geo_lat,
    		],
 			'path' => $report->path,
 			'directory' => $report->directory,
 			'full_path' => $report->path ? "{$report->directory}/resized/{$report->filename}" : asset("{$report->directory}/resized/{$report->filename}"),
 			'thumb_path' => $report->path ? "{$report->directory}/thumbnails/{$report->filename}" : asset("{$report->directory}/thumbnails/{$report->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}

	public function includeUser(CitizenReport $report){
       $user = $report->author ? : new User;
       if(is_null($user->id)){ $user->id = 0;}
       return $this->item($user, new UserTransformer);
    }
}