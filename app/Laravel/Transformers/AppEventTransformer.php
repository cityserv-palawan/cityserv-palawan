<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\AppEvent;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class AppEventTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date'
    ];

	public function transform(AppEvent $event)
	{
	    return [
	    	'id'			=> $event->id,
	    	'title'			=> $event->title,
	    	'excerpt'		=> $event->excerpt,
	    	// 'event_time'	=> $event->time_db($event->posted_at),
	    	
	    	// 'date'			=>
	    	// 	[
	    	// 		'created'	=> [
	    	// 			'date_db'		=> $event->date_db($event->created_at),
	    	// 			'month_year'	=> $event->month_year($event->created_at),
	    	// 			'time_passed'	=> $event->time_passed($event->created_at)
	    	// 		],
	    	// 	],
	    ];
	}

	public function includeDate(AppEvent $event){
        $collection = Collection::make([
			'date_db' => $event->date_db($event->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $event->month_year($event->created_at),
			'time_passed' => $event->time_passed($event->created_at),
			'timestamp' => $event->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(AppEvent $event){
		$user_id = Input::get('auth_id',0);
		$collection = Collection::make([
	    	'organizer'		=> $event->author ? "{$event->author->fname} {$event->author->lname}" : "???",
			'description'	=> $event->content,
			'location'		=> $event->venue,
	    	'geolocation'	=>
	    		[
	    			"lat"			=> $event->geo_long,
	    			"long"			=> $event->geo_lat,
	    		],
 			'path' => $event->path,
 			'directory' => $event->directory,
 			'full_path' => $event->path ? "{$event->directory}/resized/{$event->filename}" : asset("{$event->directory}/resized/{$event->filename}"),
 			'thumb_path' => $event->path ? "{$event->directory}/thumbnails/{$event->filename}" : asset("{$event->directory}/thumbnails/{$event->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}
	
}