<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\CitizenRequestLog;

class LogCitizenRequest extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->citizen_request_id = $form_data['citizen_request_id'];
		$this->data = $form_data['data'];
	}

	public function job(){
		$citizen_request = CitizenRequest::find($this->citizen_request_id);
		if($citizen_request){
			$log = new CitizenRequestLog;
			$log->citizen_request_id = $this->citizen_request_id;
			$log->fill($this->data);
			$log->save();
		}
	}

}
