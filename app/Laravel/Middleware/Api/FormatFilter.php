<?php 

namespace App\Laravel\Middleware\Api;

use Closure, Str;

class FormatFilter {

 /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
 public function handle($request, Closure $next)
 {

  if(!in_array(Str::lower($request->data_format), ["json","html"])){

    $response['msg'] = "Invalid return data format.";
    $response['status_code'] = "INVALID_FORMAT";
    $response['status'] = FALSE;
    $response_code = 406;

    return response()->json($response,$response_code);
  }

  return $next($request);
 }

}