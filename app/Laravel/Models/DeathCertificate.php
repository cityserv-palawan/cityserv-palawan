<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class DeathCertificate extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'death_certificate_request';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 
		'user_id',
		'req_lname',
		'req_fname',
		'req_address',
		'contact',
		'lname',
		'fname',
		'middle_name',
		'place_of_death',
		'date_of_death',
		'purpose',
		'number_of_copies',
		'status'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function author(){
		return $this->belongsTo("App\Laravel\Models\User",'user_id','id');
	}

	public function scopePending($query){
		return $query->where('status','pending');
	}
}