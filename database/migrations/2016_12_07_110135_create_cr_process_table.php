<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cr_process', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('cr_category_id')->default(0);
            $table->integer('cr_subcategory_id')->default(0);
            $table->string('title');
            $table->string('code');
            $table->integer('sequence')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('cr_process');
    }
}
