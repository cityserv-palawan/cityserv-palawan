<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('directory_id')->default(0);
            $table->string('name',150);
            $table->text('address')->nullable();
            $table->string('contact',50)->nullable();
            $table->string('geo_lat',50)->nullable();
            $table->string('geo_long',50)->nullable();
            $table->enum('status',["draft","published"])->default("draft");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('establishment');
    }
}
