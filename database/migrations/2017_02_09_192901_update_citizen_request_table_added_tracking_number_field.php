<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCitizenRequestTableAddedTrackingNumberField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citizen_request', function($table)
        {
            $table->string('tracking_number')->nullable()->after('code');
            $table->string('year',2)->nullable()->after('code');
            $table->integer('sequence_no')->nullable()->after('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citizen_request', function($table)
        {
            $table->dropColumn(array('sequence_no','year'));
        });
    }
}
