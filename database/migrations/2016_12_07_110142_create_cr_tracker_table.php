<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cr_tracker', function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->integer('cr_tracker_header_id')->default(0);
            // $table->integer('citizen_request_id')->default(0);
            // $table->integer('cr_category_id')->default(0);
            // $table->integer('cr_subcategory_id')->default(0);
            $table->integer('cr_process_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->string('process_title')->nullable();
            $table->string('process_code')->nullable();
            $table->dateTime('from');
            $table->dateTime('to');
            $table->integer('duration');
            $table->text('remarks')->nullable();
            $table->string('status',50)->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('cr_tracker');
    }
}
