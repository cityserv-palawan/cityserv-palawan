<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCitizenRequestTableAddedReleaseAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cr_tracker_header', function($table)
        {
            $table->decimal('release_amount',20,2)->default(0.0)->after('appointment_schedule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cr_tracker_header', function($table)
        {
            $table->dropColumn(array('release_amount'));
        });
    }
}
