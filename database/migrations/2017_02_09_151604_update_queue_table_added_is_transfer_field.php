<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQueueTableAddedIsTransferField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('queue', function($table)
        {
            $table->enum('is_transfer',['yes','no'])->default('no')->after('is_priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('queue', function($table)
        {
            $table->dropColumn(array('is_transfer'));
        });
    }
}
