<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{env('APP_TITLE',"Localhost")}} - Activation</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->
	
	@yield('page-styles')

	<!-- Theme JS files -->
	@yield('page-scripts')
	<script type="text/javascript" src="{{asset('backoffice/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/ripple.min.js')}}"></script>
	<script type="text/javascript">
		$(function(){
			$("[rel=tooltip]").tooltip();
		});
	</script>
	<!-- /theme JS files -->
</head>

<body>
	@yield('content')
</body>
</html>
