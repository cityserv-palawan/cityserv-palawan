@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-newspaper"></i> <span class="text-semibold">Polls</span> - Create a new poll.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.polls.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.polls.index')}}"> Polls</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Poll Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this announcement.</p>
				
				{{-- <div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">Title <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{old('title')}}">
						@if($errors->first('title'))
						<span class="help-block">{{$errors->first('title')}}</span>
						@endif
					</div>
				</div> --}}

				<div class="form-group {{$errors->first('question') ? 'has-error' : NULL}}">
					<label for="question" class="control-label col-lg-2 text-right">Question <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<textarea name="question" id="question" cols="5" rows="5" class="form-control summernote" placeholder="Type anything here...">{!!old('question',$poll->question)!!}</textarea>
						@if($errors->first('question'))
						<span class="help-block">{{$errors->first('question')}}</span>
						@endif
					</div>
				</div>

				<!-- Choices area -->
				<div id="choices_metadata" class="form-group {{$errors->first('choices_metadata') ? 'has-error' : NULL}}">
					<label for="" class="control-label col-lg-2 text-right">Choices</label>
					<div class="col-lg-9">
						<button class="btn btn-info btn-add" type="button"><i class="fa fa-plus" style="padding-right: 8px;"></i> Add New Choice</button>
					</div>
				</div>

				<div class="form-group {{$errors->first('start') ? 'has-error' : NULL}}">
					<label for="start" class="control-label col-lg-2 text-right">Start At <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input type="text" name="start" class="form-control daterange-single" value="{{old('start',$poll->start)}}">
						<span class="help-block">YYYY-MM-DD HH:mm</span>
						@if($errors->first('start'))
						<span class="help-block">{{$errors->first('start')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('end') ? 'has-error' : NULL}}">
					<label for="end" class="control-label col-lg-2 text-right">End At <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input type="text" name="end" class="form-control daterange-single" value="{{old('end',$poll->end)}}">
						<span class="help-block">YYYY-MM-DD HH:mm</span>
						@if($errors->first('end'))
						<span class="help-block">{{$errors->first('end')}}</span>
						@endif
					</div>
				</div>


				<div class="form-group {{$errors->first('posted_at') ? 'has-error' : NULL}}">
					<label for="posted_at" class="control-label col-lg-2 text-right">Posted At <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input type="text" name="posted_at" class="form-control daterange-single" value="{{old('posted_at',$poll->posted_at)}}">
						<span class="help-block">YYYY-MM-DD HH:mm</span>
						@if($errors->first('posted_at'))
						<span class="help-block">{{$errors->first('posted_at')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
					<label class="control-label col-lg-2 text-right">Upload thumbnail</label>
					<div class="col-lg-9">
						<input type="file" name="file" class="file-styled-primary">
						@if($errors->first('file'))
						<span class="help-block">{{$errors->first('file')}}</span>
						@endif
					</div>
				</div>

				{{-- <div class="form-group pl-10">
					<div class="checkbox col-lg-offset-2 col-lg-5">
						<label>
							<input type="checkbox" name="featured" class="styled" value="1" {{old('featured') == "1" ? 'checked' : NULL}}>
							Is featured?
						</label>
					</div>
				</div> --}}

				<div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
					<label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("status", $statuses, old('status',$poll->status), ['id' => "status", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('status'))
						<span class="help-block">{{$errors->first('status')}}</span>
						@endif
					</div>
				</div>
				
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.polls.index')}}">Cancel</a>
			</div>
		</div>

		<div style="display: none;" id="template">
			<div class="row" style="padding-bottom: 10px;">
				<div class="col-md-5">
					<input type="text" class="form-control" name="keys[]" placeholder="Enter Choice">
				</div>
				<div class="col-md-5" style="display: none;">
					{!! Form::select("values[]", ['0' => "Number"], null, ["class" => "form-control"]) !!}
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-link text-danger btn-xs btn-delete"><i class="fa fa-close"></i></button>
				</div>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
{{-- <script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script> --}}


<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
			singleDatePicker: true,
			timePicker: true,
			timePickerIncrement: 1,
			showDropdowns: true,
			locale: {
				format: 'YYYY-MM-DD h:mm A'
			}
		});

		$(".btn-add").click(function() {
	    	$(this).before($("#template").html());
	    });

		$("#choices_metadata").delegate(".btn-delete", "click", function() {
	    	$(this).parent().parent().remove();
	    });

	});
</script>
<script type="text/javascript">
	$(function(){
	    $('#question').summernote({
	    	height: 400,
	    	onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
	    });

	     function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_TOKEN','base64:Lhp1BB3ms7WVgXBKbOkSmDQaIYlCQu/sXfV1Tp2woR0=')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                	if(data.status == true){
                		$('#content').summernote('insertImage', data.image);
                	}
                }
            });
        }

	});
</script>
@stop