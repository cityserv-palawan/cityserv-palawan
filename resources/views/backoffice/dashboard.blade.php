@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><span class="text-semibold">Dashboard</span> - Overview &amp; Statistics</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
	
	@if(in_array($auth->type, ['super_user','admin','teller']))
	<h4 class="content-group">
		Quick Links
	</h4>

	<div class="row">
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.articles.create')}}" class="btn bg-info-800 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-newspaper"></i> <span>Publish new article</span></a>
			<br class="clearfix">
		</div>
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.announcements.create')}}" class="btn bg-info-800 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-megaphone"></i> <span>Publish new announcement</span></a>
			<br class="clearfix">
		</div>
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.events.create')}}" class="btn bg-info-800 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-calendar"></i> <span>Publish new event</span></a>
			<br class="clearfix">
		</div>
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.directories.create')}}" class="btn bg-info-700 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-direction"></i> <span>Add new directory</span></a>
			<br class="clearfix">
		</div>
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.admins.create')}}" class="btn bg-info-600 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-user"></i> <span>New employee account</span></a>
			<br class="clearfix">
		</div>
		{{--
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.citizen_requests.index')}}" class="btn bg-info-600 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-certificate"></i> <span>View MAC Requests</span></a>
			<br class="clearfix">
		</div>
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.citizen_requests.create')}}" class="btn bg-info-700 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-plus3"></i> <span>Create new MAC Request</span></a>
			<br class="clearfix">
		</div>
		--}}
		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
			<a href="{{route('backoffice.users.index')}}" class="btn bg-info-800 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-user"></i> <span>Manage citizen account</span></a>
			<br class="clearfix">
		</div>
	</div>

	{{--
	<h4 class="content-group">
		MAC Request
		<small class="display-block">Statistics of MAC request records.</small>
	</h4>

	<div class="row">
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body bg-slate-800 text-center">
					<div class="icon-object border-primary text-primary"><i class="icon-bell3"></i></div>
					<h1 class="text-light"><strong class="text-primary">{{$all_request->where(DB::raw("DATE(created_at)"),$today)->count()}}</strong> Today</h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body bg-slate-800 text-center">
					<div class="icon-object border-primary text-primary"><i class="icon-calendar"></i></div>
					<h1 class="text-light"><strong class="text-primary">{{$this_month}}</strong> This Month</h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body text-center">
					<div class="icon-object border-orange text-orange"><i class="icon-hour-glass"></i></div>
					<h1 class="text-light"><strong class="text-orange">{{$all_request->where('status','pending')->count()}}</strong> Overall Ongoing</h1>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body text-center">
					<div class="icon-object border-success text-success"><i class="icon-thumbs-up3"></i></div>
					<h1 class="text-light"><strong class="text-success">{{$all_request->where('status','done')->count()}}</strong> Overall Approved</h1>
				</div>
			</div>
		</div>
	</div>

	<h4 class="content-group">
		Productivity Meter
		<small class="display-block">The statistics of completed tasks per month.</small>
	</h4>

	<div class="row">
		<div class="col-md-9">
			<!-- Simple line chart -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold"></h6>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse"></a></li>
							<li><a data-action="reload"></a></li>
							<li><a data-action="close"></a></li>
						</ul>
					</div>
				</div>

				<div class="panel-body">
					<div class="chart-container">
						<div class="chart" id="c3-line-chart"></div>
					</div>
				</div>
			</div>
			<!-- /simple line chart -->
		</div>
		<div class="col-md-3">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel">
						<div class="panel-body text-center">
							<h1 class="text-light"><strong class="text-semibold"><i class="icon-alarm"></i> {{Helper::mins_to_time($average_request)}}</strong> <span class="help-block">AVG COMPLETION TIME</span></h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="panel">
						<div class="panel-body text-center">
							@if($quickest_request)
							<h1 class="text-light"><strong class="text-semibold"><i class="icon-alarm"></i>{{Helper::mins_to_time($quickest_duration)}}</strong> <span class="help-block">FASTEST COMPLETION TIME <br>(Tracking No. : <a href="{{route('backoffice.citizen_requests.edit',[$quickest_request->id])}}">{{$quickest_request->tracking_number}}</a>)</span></h1>
							@else
							<h1 class="text-light">No completed transactions yet.</h1>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<h4 class="content-group">
		MAC Statistics
		<small class="display-block">The statistics of MAC requests per category.</small>
	</h4>

	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-flat" id="pie_chart">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">MAC Requests Per Category</h6>
				</div>

				<div class="panel-body">
					<div class="chart-container text-center">
						<div class="display-inline-block" id="c3-donut-chart"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-flat" id="bar_chart">
						<div class="panel-body">
							<div class="chart-container">
								<div class="chart" id="c3-bar-chart"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	<div class="row">
		<div class="col-md-3">
			<div class="col-md-12">
				<div class="panel panel-flat  bg-slate-800">
					<div class="panel-body text-center">
						<h1 class="text-light">{{$all_request->where(DB::raw("created_at"),$today)->count()}}</h1>
						<span class="text-muted">Today's Request</span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-flat  bg-green-700">
							<div class="panel-body text-center">
								<h1 class="text-light">{{$all_request->where('status','done')->count()}}</h1>
								<span class="text-muted">Completed Request</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-flat bg-orange-700">
							<div class="panel-body text-center">
								<h1 class="text-light">{{$all_request->where('status','pending')->count()}}</h1>
								<span class="text-muted">Ongoing Request</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			@if($auth->type == "super_user")
			<div class="col-md-12">
				<div class="panel panel-flat bg-slate-800">
					<div class="panel-body text-center">
						<h1 class="text-light">{{$all_user->count()}}</h1>
						<span class="text-muted">Total Accounts</span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-body text-center">
						<h1 class="text-light">{{$all_user->where('type','user')->where(DB::raw("created_at"),$today)->count()}}</h1>
						<span class="text-muted">Today's Registered Citizen</span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-body text-center">
						<h1 class="text-light">{{$all_user->whereIn('type',['super_user','admin','employee','teller','receptionist','tv'])->count()}}</h1>
						<span class="text-muted">Total Admins</span>
					</div>
				</div>
			</div>
			
			@endif
		</div>
		<div class="col-md-9">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Visualization ( Citizen Requests )</h6>
				</div>

				<div class="panel-body">
					<div class="chart-container">
						<div class="chart" id="c3-bar-chart"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	--}}
	@else
	<h3 class="text-light">Sorry, you need to have administrator rights to view the dashboard statistics.</h3>
	@endif
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->

@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/visualization/c3/c3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/charts/c3/c3_bars_pies.js')}}"></script>

<!-- Fancybox -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/media/fancybox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/pages/gallery.js')}}"></script>
<script type="text/javascript">
	var month_list = ["x"];
	var medical = ["Medical"];
	var burial = ["Burial"];
	var scholarship = ["Scholarship"];
	var all_list = ["All Transactions"];
	var completed_list = ["Approved Transactions"];
	@foreach($month_list as $index => $value)
	month_list.push("{{$value}}");
	all_list.push("{{$all_list[$index]}}")
	completed_list.push("{{$completed_list[$index]}}")

	medical.push("{{$medical[$index]}}")
	burial.push("{{$burial[$index]}}")
	scholarship.push("{{$scholarship[$index]}}")
	@endforeach

	$(function(){

		 // Generate chart
	    var line_chart = c3.generate({
	        bindto: '#c3-line-chart',
	        size: { height: 400 },
	        color: {
	            pattern: ['#56AED9', '#636363', '#1E88E5']
	        },
	        data: {
	        	x: 'x',
	        	columns: [
	        		month_list,
	        		completed_list,
	        		all_list
		        	// ['x', '2013-04-01', '2013-05-02', '2013-06-03', '2013-07-04', '2013-08-05', '2013-09-06'],
		        	// ['data1', 30, 200, 100, 400, 150, 250],
		        	// ['data2', 130, 340, 200, 500, 250, 350]
	        	],
	        	type: 'spline'
	        },
	        axis: {
	        	x: {
	        		type: 'timeseries',
	        		tick: {
	        			format: function (x) {
	        				if((x.getMonth()+1) % 6 === 0) {
	        					return ('0' + (x.getMonth()+1)).slice(-2) + '/' + x.getFullYear().toString().substr(2,2);
	        				}
	        			}
	        		}
	        	}
	        },
	        tooltip: {
	        	format: {
	        		title: function (d) {
	        			var format = d3.time.format('%m/%y');
	        			return format(d)
	        		}
	        	}
	        },
	        grid: {
	            y: {
	                show: true
	            },
	            x: {
	            	show: true
	            }
	        }
	    });

	    // Resize chart on sidebar width change
	    $(".sidebar-control").on('click', function() {
	        line_chart.resize();
	    });


	     // Generate chart
	     var donut_chart = c3.generate({
	     	bindto: '#c3-donut-chart',
	     	size: { width: 350, height : 375 },
	     	color: {
	     		pattern: ['#37474F', '#56AED9', '#92BDD2', '#D73C02', '#8B1E1C']
	     	},
	     	data: {
	     		columns: [
	     		["Medical", {{$all_request->where('subcategory',"medical")->count()}}],
	     		["Burial", {{$all_request->where('subcategory',"burial")->count()}}],
	     		["Scholarship", {{$all_request->where('subcategory',"scholarship")->count()}}],
	     		],
	     		type : 'donut'
	     	},
	     	donut: {
	     		title: "Request % Per Category"
	     	}
	     });


	    
		// Generate chart
	    var bar_chart = c3.generate({
	        bindto: '#c3-bar-chart',
	        size: { height: 400 },
	        data: {
	     		'x' : 'x',
	            columns: [
	            	month_list,
	               	medical,
	        		burial,
	        		scholarship
	            ],
	            type: 'bar'
	        },
	        color: {
	        	pattern: ['#37474F', '#56AED9', '#92BDD2', '#D73C02', '#8B1E1C']
	        },
	        bar: {
	            width: {
	                ratio: 0.5
	            }
	        },
	        axis: {
	        	x: {
	        		type: 'timeseries',
	        		tick: {
	        			format: function (x) {
	        				if((x.getMonth()+1) % 6 === 0) {
	        					return ('0' + (x.getMonth()+1)).slice(-2) + '/' + x.getFullYear().toString().substr(2,2);
	        				}
	        			}
	        		}
	        	}
	        },
	        tooltip: {
	        	format: {
	        		title: function (d) {
	        			var format = d3.time.format('%m/%y');
	        			return format(d)
	        		}
	        	}
	        },
	        grid: {
	            y: {
	                show: true
	            },
	            x: {
	            	show: true
	            }
	        }
	    });

	});
</script>
@stop