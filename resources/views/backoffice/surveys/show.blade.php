@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-wrench"></i> <span class="text-semibold">Surveys</span> - Edit a service.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.surveys.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.surveys.index')}}"> Surveys</a></li>
			<li class="active">Show</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Service Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Respondent Profile</p>
				
				<div class="form-group ">
					<label for="title" class="control-label col-lg-2 text-right">Citizen <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{$survey->user ? "{$survey->user->fname} {$survey->user->lname}" : "???"}}" readonly="">
					</div>
				</div>

				<div class="form-group ">
					<label for="title" class="control-label col-lg-2 text-right">Citizen Request <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{$survey->tracking_number}}" readonly="">
					</div>
				</div>

				<div class="form-group ">
					<label for="title" class="control-label col-lg-2 text-right">Submitted On <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{$survey->added_on()}}" readonly="">
					</div>
				</div>
				
			</div>
		</div>

		<div class="panel ">
			<div class="panel-heading">
				<h5 class="panel-title">Result of the survey</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">

				<div class="table-responsive">
					<table class="table table-bordered table-hover" id="target-table">
						<thead>
							<tr>
								<th>Statement</th>
								<th>Strongly Disagree</th>
								<th>Somewhat Disagree</th>
								<th>Strongly Agree</th>
								<th>Somewhat Agree</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" ><span class="text-semibold">SPEED OF SERVICE DELIVERY</span></td>
							</tr>
							<tr>
								<td>
									1.	Transactions are completed within the timeframe provided for by the citizen's charter. <br>
										<small>
										Note: <br>
										<i>For simple transactions - 3 days</i> <br>
										<i>For complex transactions -  2 weeks</i>
										</small>
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 1)->first() AND $surveys->where('item_no', 1)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 1)->first() AND $surveys->where('item_no', 1)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 1)->first() AND $surveys->where('item_no', 1)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 1)->first() AND $surveys->where('item_no', 1)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									2.	The length of process can be improved
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 2)->first() AND $surveys->where('item_no', 2)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 2)->first() AND $surveys->where('item_no', 2)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 2)->first() AND $surveys->where('item_no', 2)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 2)->first() AND $surveys->where('item_no', 2)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									3.	Follow-ups were necessary to produce the documents requested.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 3)->first() AND $surveys->where('item_no', 3)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 3)->first() AND $surveys->where('item_no', 3)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 3)->first() AND $surveys->where('item_no', 3)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 3)->first() AND $surveys->where('item_no', 3)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									4.	There is immediate and timely notification for the availability of the documents requested.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 4)->first() AND $surveys->where('item_no', 4)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 4)->first() AND $surveys->where('item_no', 4)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 4)->first() AND $surveys->where('item_no', 4)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 4)->first() AND $surveys->where('item_no', 4)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td colspan="5" ><span class="text-semibold">CLARITY</span></td>
							</tr>
							<tr>
								<td>
									5.	The flow/process of the transaction is comprehensive and easy to understand.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 5)->first() AND $surveys->where('item_no', 5)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 5)->first() AND $surveys->where('item_no', 5)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 5)->first() AND $surveys->where('item_no', 5)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 5)->first() AND $surveys->where('item_no', 5)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									6.	The process is client-friendly
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 6)->first() AND $surveys->where('item_no', 6)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 6)->first() AND $surveys->where('item_no', 6)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 6)->first() AND $surveys->where('item_no', 6)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 6)->first() AND $surveys->where('item_no', 6)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									7.	Process guides are easily accessible for the clients.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 7)->first() AND $surveys->where('item_no', 7)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 7)->first() AND $surveys->where('item_no', 7)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 7)->first() AND $surveys->where('item_no', 7)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 7)->first() AND $surveys->where('item_no', 7)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td colspan="5" ><span class="text-semibold">OVERALL SERVICE</span></td>
							</tr>
							<tr>
								<td>
									8.	The documents are completely accurate.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 8)->first() AND $surveys->where('item_no', 8)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 8)->first() AND $surveys->where('item_no', 8)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 8)->first() AND $surveys->where('item_no', 8)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 8)->first() AND $surveys->where('item_no', 8)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									9.	The City Assessor’s Office handles transactions efficiently.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 9)->first() AND $surveys->where('item_no', 9)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 9)->first() AND $surveys->where('item_no', 9)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 9)->first() AND $surveys->where('item_no', 9)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 9)->first() AND $surveys->where('item_no', 9)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
							<tr>
								<td>
									10.	I am satisfied with how the City Assessor’s Office handled my transaction.
								</td>
								<td class="text-center">{!! ($surveys->where('item_no', 10)->first() AND $surveys->where('item_no', 10)->first()->answer == "strongly_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 10)->first() AND $surveys->where('item_no', 10)->first()->answer == "somewhat_disagree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 10)->first() AND $surveys->where('item_no', 10)->first()->answer == "strongly_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
								<td class="text-center">{!! ($surveys->where('item_no', 10)->first() AND $surveys->where('item_no', 10)->first()->answer == "somewhat_agree") ? "<i class='icon-check'></i>" : NULL !!}</td>
							</tr>
						</tbody>
					</table>
				</div>

				<br>

				<h6 class="text-semibold">Qualitative</h6>

				<br>
					
				<p class="text-semibold">What were the specific difficulties you encountered during the transaction? How was it addressed by the agency concerned?</p>
				<p>{!! $surveys->where('item_no', 11)->first() ? "<u>" . $surveys->where('item_no', 11)->first()->answer . "</u>" : "---" !!}</p> <br> 

				<p class="text-semibold">What are the specific innovations that you want the agency to adapt to better the service provision?</p>
				<p>{!! $surveys->where('item_no', 12)->first() ? "<u>" . $surveys->where('item_no', 12)->first()->answer . "</u>" : "---" !!}</p> <br>

				<p class="text-semibold">What are other important things that you want to be addressed to better improve the service delivery?</p>
				<p>{!! $surveys->where('item_no', 13)->first() ? "<u>" . $surveys->where('item_no', 13)->first()->answer . "</u>" : "---" !!}</p> <br>

				<p class="text-semibold">How do you feel about the entire process?</p>
				<p>{!! $surveys->where('item_no', 14)->first() ? "<u>" . $surveys->where('item_no', 14)->first()->answer . "</u>" : "---" !!}</p> <br>
				
			</div>


		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.surveys.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	});
</script>
@stop