@extends("backoffice._template.queue")

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-8">
			<div class="category-header">
				<h4 class="content-group text-semibold">Create a Request <small class="display-block">Choose a service to create generate new number</small></h4>
			</div>
			<div class="category-content">
				<div class="row">
					<div class="col-xs-4">
						<button class="btn bg-pink-300 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-accessibility2"></i> <span>Social Service</span></button>
						<button class="btn bg-purple-300 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-pulse2"></i> <span>Health Service</span></button>
					</div>
					
					<div class="col-xs-4">
						<button class="btn bg-warning-300 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-books"></i> <span>Scholarship</span></button>
						<button class="btn bg-brown-300 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-balance"></i> <span>Legal Service</span></button>
					</div>
					<div class="col-xs-4">
						<button class="btn bg-slate-300 btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-people"></i> <span>Administrative</span></button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Recently Queued</h6>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="panel-body">
					<ul class="media-list media-list-bordered">
						@foreach(range(1,5) as $index => $value)
						<li class="media">
							<div class="media-body">
								<h6 class="media-heading"><strong>SOCIAL-00{{$value}}</strong></h6>
								<span>Social Services</span>
							</div>
							<div class="media-right">
								<a href="">
									<span class="label label-flat label-rounded label-icon border-pink text-pink-300"><i class="icon-accessibility2"></i></span>
								</a>
							</div>
						</li>
						@endforeach
					</ul>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<span class="heading-text text-semibold">Overall Total:</span>
						<span class="heading-text pull-right"><span class="badge bg-success-400 position-left">38</span></span>
					</div>
					<div class="heading-elements">
						<span class="heading-text text-semibold">Most Requested : Social Service</span>
						<span class="heading-text pull-right"><span class="badge bg-success-400 position-left">38</span></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop