@if(Session::has('notification-status'))
<script type="text/javascript" src="{{asset('backoffice/js/plugins/notifications/noty.min.js')}}"></script>
<script type="text/javascript">

    var type = "";

    @if(Session::get('notification-status') == "failed")
        type = "danger";
    @endif

    @if(Session::get('notification-status') == "success")
        type = "success";
    @endif

    @if(Session::get('notification-status') == "warning")
        type = "warning";
    @endif

    @if(Session::get('notification-status') == "info")
        type = "info";
    @endif

    $(function(){
        noty({
            width: 200,
            text: "{{Session::get('notification-msg')}}",
            type: type,
            dismissQueue: true,
            timeout: 4000,
        });
        return false;
    })
</script>
@endif